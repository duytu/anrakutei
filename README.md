## Setup project
- Prepare env: `cp .env.sample .env`
- Install dependencies: `yarn | npm run install`

## Start project
- Run `yarn start | npm run start`
