import Menu from './pages/Menu';
import './App.scss';

function App() {
  return (
    <div className="App">
      <Menu />
    </div>
  );
}

export default App;
