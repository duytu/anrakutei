import { useEffect, useState, useRef } from "react";
import { Tabs, Swiper } from 'antd-mobile';
import { values } from 'lodash';
import Genre from '../../components/Genre';
import './index.scss';

export default function (props) {
  const [activeTab, setActiveTab] = useState(0);
  const [menuInfo, setMenuInfo] = useState({});
  const [categories, setCategories] = useState([]);
  const swiperRef = useRef(null);

  useEffect(() => {
    fetch('data.json')
      .then(res => res.json())
      .then(res => {
        if (!res || !res.data || !res.data.menu_info) {
          throw  new Error('Fetch data failed!')
        }

        const categoriesData = values(res.data.menu_info.category);
        setMenuInfo(res.data.menu_info);
        setCategories(categoriesData);
      })
      .catch(err => console.error(err));
  }, []);

  const changeTab = key => {
    setActiveTab(key);
    if (swiperRef && swiperRef.current) {
      swiperRef.current.swipeTo(key);
    }
  };

  return (
    <>
      <Tabs activeKey={activeTab} onChange={key => changeTab(key)}>
        {(categories || []).map((category, index) => {
          const { categorynm } = category;
          return (
            <Tabs.Tab title={categorynm || '--'} key={index}>
              {categorynm}
            </Tabs.Tab>
          )
        })}
      </Tabs>
      <Swiper
        defaultIndex={activeTab}
        onIndexChange={index => setActiveTab(index)}
        ref={swiperRef}
      >
        {(categories || []).map((category, index) => {
          const { categorycd } = category;
          return (
            <Swiper.Item key={index}>
              <Genre
                category={category}
                categoryGenres={menuInfo && menuInfo.menu_sub && menuInfo.menu_sub[categorycd] || {}}
                genres={menuInfo && menuInfo.genre || {}}
                menuItems={menuInfo && menuInfo.menu_detail || {}}
              />
            </Swiper.Item>
          )
        })}
      </Swiper>
    </>
  )
}