import { Image } from 'antd-mobile';
import './index.scss';

export default (props) => {
  const { menuItem } = props;
  const { price, img, lang } = menuItem;
  const name = lang && lang.jp || '--';
  const imageUrl = `${process.env.REACT_APP_IMAGE_SERVRE_URL}${img?.jp?.filenm}`;
  const getPriceString = (price, currency, lang) => {
    const formatter = new Intl.NumberFormat(lang, {
      style: 'currency',
      currency: currency
    });

    return formatter.format(price);
  }

  return (
    <div className='MenuItem'>
      <Image src={imageUrl} width="100%" height={100} fit='contain' lazy onError={console.log} />
      <footer>
        <span className="name">{name || '--'}</span>
        <span className="price">{price && getPriceString(price, 'JPY', 'en') || '0'}</span>
      </footer>
    </div>
  )
}
