import { IndexBar, List } from 'antd-mobile';
import { keys } from 'lodash';
import MenuItem from '../MenuItem';
import './index.scss';

export default (props) => {
  const { categoryGenres, genres, menuItems } = props;
  const genreIds = keys(categoryGenres);
  return (
    <div style={{ height: window.innerHeight }}>
      <IndexBar sticky>
        {(genreIds || []).map(genreId => {
          const genre = genres[genreId];
          const { code, name } = genre
          const { menu } = categoryGenres[genreId];
          return (
            <IndexBar.Panel
              index={name}
              title={name}
              key={code}
            >
              <List mode="card">
                {(menu || []).map((menuId, index) => {
                  const menuItem = menuItems[menuId];
                  return (
                    <List.Item key={index}>
                      <MenuItem menuItem={menuItem} />  
                    </List.Item>
                  )
                })}
              </List>
            </IndexBar.Panel>
          )
        })}
      </IndexBar>
    </div>
  )
}